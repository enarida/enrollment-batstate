package com.imergex.enrollmentregtration

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import kotlinx.android.synthetic.main.activity_main.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase



class MainActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null //Firebase auth variable
    var mDatabase: DatabaseReference? = null //Firebase database variable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mAuth = FirebaseAuth.getInstance() //initialize auth
        mDatabase = FirebaseDatabase.getInstance().reference //initialize database

        setupUI()
    }

    fun setupUI() {
        btn_register.setOnClickListener {
            validator()
        }
        btn_add.setOnClickListener {
            addItem()
        }
    }

    fun validator() {
        txt_password.validator()
            .nonEmpty()
            .addErrorCallback {
                txt_password.error = it
            }
            .addSuccessCallback { // if validator is successfull
                val email = txt_email.text.toString()
                val password = txt_password.text.toString()

                // create user with email
                mAuth?.createUserWithEmailAndPassword(email, password)?.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = mAuth?.currentUser?.email //get current email user
                        Toast.makeText(this, "Welcome $user", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                    }
                }
            }
            .check()
    }

    fun addItem() {

        // Write a message to the database
        val user = UserItem.create()
        user.course = txt_addItem.text.toString()
        user.email = txt_email.text.toString()
        user.name = txt_firstName.text.toString()

        //We first make a push so that a new item is made with a unique ID
        val newItem = mDatabase?.child("users")?.push()
        newItem?.setValue(user)
        Toast.makeText(this, "POSTED", Toast.LENGTH_SHORT).show()
    }
}
